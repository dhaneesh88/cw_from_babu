
# How-to upload prototypes
When protos are done, you can upload them to the stage git repository.
- Repo: https://github.com/mobomo/canwd/tree/dev
- Prototypes: http://candw.dev.mobomo.com/html/

## Step-by-step guide

1. git clone the repository on your machine:
    ```    
    $ git clone git@github.com:mobomo/canwd.git
    ```
    Yep... as you can see, we had a typo when creating the repo :S so its called "canwd" instead of "candw". We decided to leave it as it will be only for dev, no prod.

2. Once you have a local copy of the repo, cd into your repo root and checkout the dev branch:
    ```
    $ git checkout dev
    ```
    **Make sure you are on dev!** Otherwise the html directory won't be there.
3. If you check the directories on the repo root, you will see a 'docroot' and 'html' dirs.
  You should see something like this:
  ```
  user@localhost:/path/to/local-repo/canwd (dev) $ ls -l
  total 12
  drwxrwxr-x 10 user group 4096 Oct 26 17:43 docroot
  drwxrwxr-x  2 user group 4096 Oct 26 17:43 html
  -rw-rw-r--  1 user group    7 Oct 26 17:43 README.md
  ```

4. Place the prototypes inside **html** directory. You will see a home.html example file in there, you can override it.

5. Edit the index.html file and ponit to the new files you have added. Use the added example as reference.
6. Follow the common git procedure to add, commit and push your changes to the repository.

    ``` bash
    $ git add /path/to/canwd/html
    $ git commit -m 'adding html prototypes'
    $ git push
    ```
7. Once you pushed your changes you can verify everything is looking as expected on: http://candw.dev.mobomo.com/html/ 
    
So, for example, the home html file you added will be accessible in: http://candw.dev.mobomo.com/html/home.html


**Please do not add any sensitive information** (like users and passwords) into the prototypes as the html directory sits under the public_html dir so they will be accessible from the internet.
