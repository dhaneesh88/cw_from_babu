$(document).ready(function () {
    $(document).foundation();

    $("#industry-slider").owlCarousel({
        items: 3,
        stagePadding: 10,
        margin: 30,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 2
            },
            1000: {
                items: 3
            }
        }
    });

    $("#practise-slider").owlCarousel({
        items: 3,
        stagePadding: 10,
        margin: 30,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 3
            },
            1000: {
                items: 5
            }
        }
    });

    // Custom Navigation Events
    $("#industry-next").click(function () {
        $("#industry-slider").trigger('next.owl.carousel');
    })
    $("#industry-prev").click(function () {
        $("#industry-slider").trigger('prev.owl.carousel');
    })
    $("#practise-next").click(function () {
        $("#practise-slider").trigger('next.owl.carousel');
    })
    $("#practise-prev").click(function () {
        $("#practise-slider").trigger('prev.owl.carousel');
    })

    $(".page-contact-open .inputs input").bind("keyup", function (e) {
        if ($(this).val() == '') {
            $(this).next().show();
        } else {
            $(this).next().hide();
        }
    })


    var $item = $('.scrollItem'),
        visible = 1,
        index = 0,
        endIndex = $item.length - 1;

    $('#arrowRight').click(function () {
        if (index < endIndex) {
            index++;
            $item.animate({ 'left': '-=' + (window.innerWidth - 45) });
        }
    });

    $('#arrowLeft').click(function () {
        if (index > 0) {
            index--;
            $item.animate({ 'left': '+=' + (window.innerWidth - 45) });
        }
    });
});