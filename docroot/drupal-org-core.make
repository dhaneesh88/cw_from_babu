; Drofile - Install profile
; v7.x-1.0

api = 2
core = 7.x


; If patches are needed
; ---------
; projects[drupal][patch][] = "http://cgit.drupalcode.org/ais/plain/ais.htaccess.patch"

projects[] = drupal

; Projects
; --------

projects[] = admin_menu
projects[] = admin_views
projects[] = adminimal_theme
projects[] = administerusersbyrole
projects[] = backup_migrate
projects[] = bean
projects[] = better_exposed_filters
projects[] = breakpoints
projects[] = chain_menu_access
projects[] = ckeditor
projects[] = ckeditor_link
projects[] = ctools
projects[] = date
projects[] = devel
projects[] = eck
projects[] = entity
projects[] = entityreference
projects[] = features
projects[] = field_formatter_settings
projects[] = field_group
projects[] = field_tools
projects[] = google_analytics
projects[] = hide_formats
projects[] = image
projects[] = inline_entity_form
projects[] = jquery_update
projects[] = libraries
projects[] = link
projects[] = menu_admin_per_menu
projects[] = menu_attributes
projects[] = menu_block
projects[] = menu_position
projects[] = metatag
projects[] = module_filter
projects[] = multiupload_imagefield_widget
projects[] = multiupload_filefield_widget
projects[] = panels
projects[] = pathauto
projects[] = permission_select
projects[] = picture
projects[] = publishcontent
projects[] = redirect
projects[] = rename_admin_paths
projects[] = role_delegation
projects[] = schemaorg
projects[] = site_map
projects[] = smart_trim
projects[] = stage_file_proxy
projects[] = strongarm
projects[] = token
projects[] = view_unpublished
projects[] = views
projects[] = views_bulk_operations
projects[] = xmlsitemap

; Dev modules
; --------
projects[semantic_panels][version] = "1.x-dev"
projects[semantic_panels][download][type] = "git"
projects[semantic_panels][download][url] = "git://git.drupal.org/project/semantic_panels.git"
projects[semantic_panels][download][revision] = "0bd3b59a2ac9f91ec6ddf5b3ada2c9073dbc51d3"


; Custom modules
; --------
projects[drofile_tweaks][type] = "module" 
projects[drofile_tweaks][download][type] = "git" 
projects[drofile_tweaks][download][url] = "git@gitlab.com:drofile/drofile_tweaks.git" 
projects[drofile_tweaks][subdir] = "custom"

; Profiles
; --------
projects[drofile_profile][type] = "profile"
projects[drofile_profile][download][type] = "git"
projects[drofile_profile][download][url] = "git@gitlab.com:drofile/drofile_profile.git"
projects[drofile_profile][subdir] = ""
projects[drofile_profile][destination] = "profiles"

; Themes
; --------
projects[drubath][type] = "theme"
projects[drubath][download][type] = "git"
projects[drubath][download][url] = "git@gitlab.com:drofile/drubath.git"
projects[drubath][subdir] = ""
projects[drubath][destination] = "themes"

defaults[projects][subdir] = "contrib"

; Libraries
; ---------

; CKEditor
libraries[ckeditor][download][type]= "get"
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%204.4.4/ckeditor_4.4.4_full.zip"
libraries[ckeditor][directory_name] = "ckeditor"
libraries[ckeditor][destination] = "libraries"
