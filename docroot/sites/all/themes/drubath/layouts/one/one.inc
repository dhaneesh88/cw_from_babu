<?php

// Plugin definition
$plugin = array(
  'title' => t('One column - drubath'),
  'category' => t('Columns: 1'),
  'icon' => 'one.png',
  'theme' => 'one',
  'regions' => array(
  	'main' => t('Main content')
  ),
);
