<?php

// Plugin definition
$plugin = array(
  'title' => t('Two columns - drubath'),
  'category' => t('Columns: 2'),
  'icon' => 'two.png',
  'theme' => 'two',
  'regions' => array(
  	'main' => t('Main content')
  ),
);
