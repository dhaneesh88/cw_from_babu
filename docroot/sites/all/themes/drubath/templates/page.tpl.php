<header id="header" role="banner">
	<div id="header-inner" class="row">  

    <div class="large-12 columns">

      <div id="site-name">
				<a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home">drubath</a>
			</div>

      <a id="nav-toggle" class="show-for-mobile">Toggle menu</a>
		
      <nav id="navigation" role="navigation">
        <?php print theme('links__system_main_menu', array('links' => $main_menu));?>
    	</nav>

      <?php print render($page['header']);?>

      <?php if(isset($emergencyMessage)):?>
        <?php print $emergencyMessage; ?>
      <?php endif;?>

  	</div>
		
  </div>
</header>

<div id="wrapper" class="animated fadeIn">

  <?php $tab_rendered = render($tabs); ?>
  <?php if ($page['highlighted'] || $messages || !empty($tab_rendered) || $action_links): ?>
    <div class="row">
      <div class="large-12 columns" id="admin-functions">
        <?php if ($page['highlighted']): ?>
          <div id="highlighted"><?php print render($page['highlighted']); ?></div>
        <?php endif; ?>
        <?php print $messages; ?>
        <?php if (!empty($tab_rendered)): ?><div class="tabs clearfix"><?php print $tab_rendered; ?></div><?php endif; ?>
        <?php print render($page['help']); ?>
        <?php if ($action_links): ?>
          <ul class="action-links"><?php print render($action_links); ?></ul>
        <?php endif; ?>
      </div>
    </div>
  <?php endif; ?>

  <div class="row">
    <div class="large-12 columns">
      <?php print render($page['content']); ?>
    </div>
  </div>
  	
  <footer id="footer" role="contentinfo">
    <div class="row">
      <div class="large-12 columns">
          <?php print render($page['footer']); ?>
      </div>
    </div>
  </footer>
  
  <?php if(isset($page['custom_copyright'])):?>
    <?php print render($page['custom_copyright']);?>
  <?php endif;?>

</div>

<div class="hidden">
  <?php print render($page['hidden']); ?>
</div>