<?php
/**
 * @file
 * Default theme implementation for entities.
 *
 * Available variables:
 * - $content: An array of comment items. Use render($content) to print them all, or
 *   print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $title: The (sanitized) entity label.
 * - $url: Direct url of the current entity if specified.
 * - $page: Flag for the full page state.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. By default the following classes are available, where
 *   the parts enclosed by {} are replaced by the appropriate values:
 *   - entity-{ENTITY_TYPE}
 *   - {ENTITY_TYPE}-{BUNDLE}
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */
?>
  <?php if (!empty($content['field_representative_matters'])) : ?>
    <div class="tabs-panel" id="panel2">
      <?php print render($content['field_representative_matters']);?>
    </div>
  <?php endif ;?>

  <?php if (!empty($content['field_practice_areas'])) : ?>
    <div class="tabs-panel" id="panel3">
      <?php print render($content['field_practice_areas']);?>
    </div>
  <?php endif ;?>

  <?php if (!empty($content['field_education'])) : ?>
    <div class="tabs-panel" id="panel4">
      <?php print render($content['field_education']);?>
    </div>
  <?php endif ;?>

  <?php if (!empty($content['field_news_and_insights'])) : ?>
    <div class="tabs-panel" id="panel5">
      <?php print render($content['field_news_and_insights']);?>
    </div>
  <?php endif ;?>

