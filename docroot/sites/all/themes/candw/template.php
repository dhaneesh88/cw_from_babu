<?php

/**
 * Changes the default meta content-type tag to the shorter HTML5 version
 */
function candw_html_head_alter(&$head_elements) {
  $head_elements['system_meta_content_type']['#attributes'] = array(
    'charset' => 'utf-8'
  );
}


/**
 * Changes the search form to use the HTML5 "search" input attribute
 */
function candw_preprocess_search_block_form(&$vars) {
  $vars['search_form'] = str_replace('type="text"', 'type="search"', $vars['search_form']);
  
}
/**
* hook_form_FORM_ID_alter
*/
function candw_form_search_block_form_alter(&$form, &$form_state, $form_id) {
    $form['search_block_form']['#prefix'] = '<span class="search-back"><i class="fa fa-chevron-left" aria-hidden="true"></i></span>';
    $form['search_block_form']['#title'] = t('Search'); // Change the text on the label element
    $form['search_block_form']['#title_display'] = 'invisible'; // Toggle label visibilty
    // Alternative (HTML5) placeholder attribute instead of using javascript
    $form['search_block_form']['#attributes']['placeholder'] = t('Search');
//     $form['actions']['#attributes']['class'][] = 'element-invisible';
    $form['actions']['submit']['#type'] = 'image_button';
    $form['actions']['submit']['#src'] =  base_path() . path_to_theme() . '/images/search-icon.png';

}

/**
 * Hook_preprocess_html()
 */
function candw_preprocess_html(&$vars) {

  // Enable swipebox
  if (theme_get_setting('swipebox')):
    drupal_add_js(path_to_theme() . '/js/libs/jquery.swipebox.min.js', array( 'weight' => 0));
    drupal_add_js(array('swipebox_enabled' => TRUE), 'setting');
    drupal_add_css(path_to_theme() . '/css/swipebox.css', array('group' => CSS_THEME, 'weight' => 1));
  endif;

  // Enable slick
  if (theme_get_setting('slick')):
    drupal_add_js(path_to_theme() . '/js/libs/slick.min.js', array( 'weight' => 0));
    drupal_add_js(array('slick_enabled' => TRUE), 'setting');
    drupal_add_css(path_to_theme() . '/css/slick.css', array('group' => CSS_THEME, 'weight' => 0));
  endif;

  // Enable matchHeight
  if (theme_get_setting('matchHeight')):
    drupal_add_js(path_to_theme() . '/js/libs/jquery.matchHeight-min.js', array( 'weight' => 0));
    drupal_add_js(array('matchHeight_enabled' => TRUE), 'setting');
  endif;

  // If plugins reaquired, load init file
  if (theme_get_setting('swipebox') || theme_get_setting('slick') || theme_get_setting('matchHeight')) {
    drupal_add_js(path_to_theme() . '/js/candw.plugins.js', array( 'weight' => 0));
  }

  // If home page
  if($vars['is_front']) {
    drupal_add_js(path_to_theme() . '/js/candw.front.js');
  }
  
  // Ensure that the $vars['rdf'] variable is an object.
  if (!isset($vars['rdf']) || !is_object($vars['rdf'])) {
    $vars['rdf'] = new StdClass();
  }

  if (module_exists('rdf')) {
    $vars['doctype'] = '<!DOCTYPE html PUBLIC "-//W3C//DTD HTML+RDFa 1.1//EN">' . "\n";
    $vars['rdf']->version = 'version="HTML+RDFa 1.1"';
    $vars['rdf']->namespaces = $vars['rdf_namespaces'];
    $vars['rdf']->profile = ' profile="' . $vars['grddl_profile'] . '"';
  } else {
    $vars['doctype'] = '<!DOCTYPE html>' . "\n";
    $vars['rdf']->version = '';
    $vars['rdf']->namespaces = '';
    $vars['rdf']->profile = '';
  }
  $node = menu_get_object();
  if ($node && isset($node->nid)) {

      if ($node->type == 'landing') {
        if (!empty($node->field_content_to_list) && ($node->field_content_to_list['und'][0]['tid'] == 38)) {
          $vars['classes_array'][] = 'page-offices';
        }
        if (!empty($node->field_content_to_list) && ($node->field_content_to_list['und'][0]['tid'] == 12)) {
          $vars['classes_array'][] = 'page-attorneys';
        }
        if (!empty($node->field_content_to_list) && ($node->field_content_to_list['und'][0]['tid'] == 15)) {
          $vars['classes_array'][] = 'page-industries';
        }
        if (!empty($node->field_content_to_list) && ($node->field_content_to_list['und'][0]['tid'] == 39)) {
          $vars['classes_array'][] = 'page-practise-areas';
        }
        if (!empty($node->field_content_to_list) && ($node->field_content_to_list['und'][0]['tid'] == 14)) {
          $vars['classes_array'][] = 'page-news';
        }
        if (!empty($node->field_content_to_list) && ($node->field_content_to_list['und'][0]['tid'] == 13)) {
          $vars['classes_array'][] = 'page-journals';
        }
        if (!empty($node->field_content_to_list) && ($node->field_content_to_list['und'][0]['tid'] == 41)) {
          $vars['classes_array'][] = 'page-careers';
        }
    }
    if ($node->type == 'attorney') {
          $vars['classes_array'][] = 'page-attorneys-open';
    }
    if ($node->type == 'page') {
          $vars['classes_array'][] = 'page-about-us';
    }
    if ($node->type == 'news') {
          $vars['classes_array'][] = 'page-news-detail';
    }
    if ($node->type == 'industry') {
          $vars['classes_array'][] = 'page-industry';
    }
    if ($node->type == 'journal') {
          $vars['classes_array'][] = 'page-journals-open';
    }
    if ($node->type == 'careers') {
          $vars['classes_array'][] = 'page-careers-open';
    }
  }
}

function candw_preprocess_one(&$variables) {
  if(!empty(menu_get_object())) {
	$node = menu_get_object();
	$variables['nodeCT'] = $node->type;
  }
}

/**
 * Returns HTML for a list or nested list of items.
 *
 * This is a clone of the core theme_item_list() function with the div class="item-list" removed.
 */
function candw_item_list($variables) {
// dpm($variables);
  $items = $variables['items'];
  $title = $variables['title'];
  $type = $variables['type'];
  $attributes = $variables['attributes'];

  $output = '';
  if (isset($title)) {
    $output .= '<h3>' . $title . '</h3>';
  }

  if (!empty($items)) {
    $output .= "<$type" . drupal_attributes($attributes) . '>';
    $num_items = count($items);
    foreach ($items as $i => $item) {
      $attributes = array();
      $children = array();
      $data = '';
      if (is_array($item)) {
        foreach ($item as $key => $value) {
          if ($key == 'data') {
            $data = $value;
          }
          elseif ($key == 'children') {
            $children = $value;
          }
          else {
            $attributes[$key] = $value;
          }
        }
      }
      else {
        $data = $item;
      }
      if (count($children) > 0) {
        // Render nested list.
        $data .= theme_item_list(array('items' => $children, 'title' => NULL, 'type' => $type, 'attributes' => $attributes));
      }
      if ($i == 0) {
        $attributes['class'][] = 'first';
      }
      if ($i == $num_items - 1) {
        $attributes['class'][] = 'last';
      }
      $output .= '<li' . drupal_attributes($attributes) . '>' . $data . "</li>\n";
    }
    $output .= "</$type>";
  }
  return $output;
}

/**
 * Process variables for menu-block-wrapper.tpl.php.
 *
 * @see menu-block-wrapper.tpl.php
 */
function candw_preprocess_menu_block_wrapper(&$variables) {
  $variables['classes_array'][] = 'menu-block-' . $variables['delta'];
  $variables['classes_array'][] = 'menu-name-' . $variables['config']['menu_name'];
  $variables['classes_array'][] = 'parent-mlid-' . $variables['config']['parent_mlid'];
  $variables['classes_array'][] = 'menu-level-' . $variables['config']['level'];
}

/**
* Implementation of hook_preprocess_page().
* Different page.tpl.php for panels pages.
*/
function candw_preprocess_page(&$vars) {
  // if this is a panel page, add template suggestions
  if($panel_page = page_manager_get_current_page()) {
    $vars['theme_hook_suggestions'][] = 'page__panel';
  }
  // page.tpl per content type
  if (isset($vars['node']->type)) { 
    $vars['theme_hook_suggestions'][] = 'page__' . $vars['node']->type; 
  }
  // Copyright text
  if (theme_get_setting('copyright')):
    $year = date('Y');
    $vars['page']['custom_copyright'] = '&copy; '. $year . ' ' . theme_get_setting('copyright');
  endif;

  // Emergency message
  $emBlock = block_load('bean', 'emergency-message');
  $em_render_array = _block_get_renderable_array(_block_render_blocks(array($emBlock)));
  $vars['emergencyMessage'] = render($em_render_array);

}


/**
* Implementation of hook_preprocess_block().
*/
function candw_preprocess_block(&$variables) {
  // tpl for Emergency message blocks
  if ($variables['block']->module == 'bean' && $variables['block']->delta == 'emergency-message') {    
    $variables['theme_hook_suggestions'][] = 'block__bean__emergency_message';
  }
}

/**
* Customise Main Menu output.
*/
// function candw_menu_link($variables) {
//  $element = $variables['element'];
//   $html = '';
//   $seeall = '';
//   if ($element['#below']) {
//       $parentlink = drupal_get_path_alias($variables['element']['#href']);
//       $seeall = '<li class="see-all absolute"><a href="/' . $parentlink . '">See All</a></li>';
//       $html = '<ul class="menu vertical">';
//       foreach ($variables['element']['#below'] as $link) {
//         if (isset($link['#href'])){
//           $path = drupal_get_path_alias($link['#href']);
//           $html .= '<li><a href="/'.$path.'">'.$link['#title'].'</a></li>';
//         }
//       }
//       $html .= $seeall . '</ul>';
//       $element['#localized_options']['attributes']['class'] = '';
//     }
//     $output = l($element['#title'], $element['#href'], $element['#localized_options']);
//     return '<li>' . $output . $html . "</li>\n";
// }

/**
* Customise Main Menu output.
*/
function candw_links__system_main_menu($variables) {
  $html = '<ul class="vertical medium-horizontal menu" data-dropdown-menu>';

  foreach ($variables['links'] as $link) {
    $classes = '';
    if(isset($link['attributes']['class'])){
      foreach($link['attributes']['class'] as $key => $class) {
        $classes .= $class . ' ';
      }
    }
    $path = drupal_get_path_alias($link['href']);
    $html .= '<li><a class="'. $classes . '" href="/'.$path.'">'.$link['title'].'</a></li>';
  }

  $html .= '<li class="search-icon"><img src="' . base_path() . path_to_theme() . '/images/search-icon.png"></li></ul>';

  return $html;
}
function candw_menu_link__menu_block($variables) {
    $element = $variables['element'];
    $output = l($element['#title'], $element['#href']);
    return '<li>' . $output . "</li>\n";
}

/**
 * Add responsive image class to all images using images styles
 */
function candw_preprocess_image_style(&$vars) {
    $vars['attributes']['class'][] = 'img-responsive';
}

/**
 * Override theme_date_display_single().
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 */
function candw_date_display_single($variables) {
  if($variables['dates']['format'] == "d M"){
    $date = $variables['dates']['value']['formatted_iso'];
    $timezone = $variables['timezone'];
    $attributes = $variables['attributes'];
    // Set vars
    $day = format_date(strtotime($date), 'custom', 'd');
    $month = format_date(strtotime($date), 'custom', 'M');

    // Wrap the result
    return '<div class="date-display-single"' . drupal_attributes($attributes) . '>' . '<h5>' . $day . '</h5>' . '<span>' . $month . '</span>' . $timezone . '</div>';
  } else {
    $date = $variables['date'];
    $timezone = $variables['timezone'];
    $attributes = $variables['attributes'];
    return '<span class="date-display-single"' . drupal_attributes($attributes) . '>' . $date . $timezone . '</span>';
  }
}

