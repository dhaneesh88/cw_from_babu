<?php

/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct url of the current node.
 * - $display_submitted: whether submission information should be displayed.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type, i.e., "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type, i.e. story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode, e.g. 'full', 'teaser'...
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined, e.g. $node->body becomes $body. When needing to access
 * a field's raw values, developers/themers are strongly encouraged to use these
 * variables. Otherwise they will have to explicitly specify the desired field
 * language, e.g. $node->body['en'], thus overriding any language negotiation
 * rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 */
?>
<?php if ($view_mode == 'listing'): ?>

<div class="grid small-12 medium-6 large-3 columns">
    <div class="grid-section">
        <div class="img-section relative">
            <?php print render($content['field_image']);?>
        </div>
        <div class="content matchHeight">
            <h5><a href="<?php print $node_url;?>"><?php print $title;?></a></h5>
            <p>
              <?php print render($content['field_position']);?>
              <?php if (!empty($content['field_office'])) : ?> - <?php endif;?>
              <?php print render($content['field_office']);?>
            </p>
        </div>
    </div>
</div>

<?php elseif ($view_mode == 'teaser'): ?>

  <div class="slider-item">
      <div class="grid">
          <div class="grid-section">
              <div class="img-section relative">
                  <?php print render($content['field_image']);?>
              </div>
              <div class="content">
                  <h5><a href="<?php print $node_url;?>"><?php print $title;?></a></h5>
                  <p>
                    <?php print render($content['field_position']);?>
                    <?php if (!empty($content['field_office'])) : ?> - <?php endif;?>
                    <?php print render($content['field_office']);?>
                  </p>
              </div>
          </div>
      </div>
  </div>

<?php elseif ($view_mode == 'journal'): ?>


  <div class="img-section"><?php print render($content['field_image']);?></div>
  <div class="details">
      <h5><?php print $title;?></h5>
      <span><?php if (!empty($content['field_position'])) :?><?php print render($content['field_position']);?> at <?php endif;?><?php print render($content['field_office']);?></span>
  </div>

<?php elseif ($view_mode == 'related'): ?>

    <div class="img-section">
        <?php print render($content['field_image']);?>
    </div>
    <div class="name m-b-30">
        <span><a href="<?php print $node_url;?>"><?php print $title;?></a></span>
    </div>

<?php elseif ($view_mode == 'banner'): ?>

<div class="banner-image">
  <?php print render($content['field_banner_image']);?>
</div>
<?php if (!empty($content['body']['#items'][0]['safe_summary'])) : ?>
  <?php print ($content['body']['#items'][0]['safe_summary']);?>
<?php endif ;?>

<?php else: ?>
<div class="content-wrapper">
  <div class="row top-content max-width">
    <div class="grid large-4 medium-6 small-12 columns img-section">
        <?php print render($content['field_image']);?>
    </div>
    <div class="grid large-8 medium-6 small-12 columns">
        <div class="content">
            <h2><?php print $title;?></h2>
            <?php if (!empty($content['body']['#items'][0]['safe_summary'])) : ?>
              <?php print ($content['body']['#items'][0]['safe_summary']);?>
            <?php endif ;?>
        </div>
    </div>
  </div>
  <div class="tabs-wrap row max-width">
      <ul class="tabs vertical columns large-4 medium-5 small-12" data-tabs id="attorney-tabs">
          <li class="tabs-title is-active"><a href="#panel1" aria-selected="true"><?php print t('Bio');?></a></li>
          <li class="tabs-title"><a href="#panel2"><?php print t('Representative Matters');?></a></li>
          <li class="tabs-title"><a href="#panel3"><?php print t('Practice Areas');?></a></li>
          <li class="tabs-title"><a href="#panel4"><?php print t('Education');?></a></li>
          <li class="tabs-title"><a href="#panel5"><?php print t('News and Insights');?></a></li>
      </ul>
      <div class="tabs-content columns large-8 medium-7 small-12" data-tabs-content="attorney-tabs">
          <?php if (!empty($content['body'])) : ?>
            <div class="tabs-panel is-active" id="panel1">
              <?php print render($content['body']);?>
            </div>
          <?php endif ;?>
          <?php if (!empty($content['field_representative_matters'])) : ?>
            <div class="tabs-panel" id="panel2">
              <?php print render($content['field_representative_matters']);?>
            </div>
          <?php endif ;?>

          <?php if (!empty($content['field_practice_areas'])) : ?>
            <div class="tabs-panel" id="panel3">
              <?php print render($content['field_practice_areas']);?>
            </div>
          <?php endif ;?>

          <?php if (!empty($content['field_education']) || !empty($content['field_law_school'])) : ?>
            <div class="tabs-panel" id="panel4">
              <h3><?php print render($content['field_law_school']);?></h3>
              <?php print render($content['field_education']);?>
            </div>
          <?php endif ;?>

          <?php if (!empty($content['field_news_and_insights'])) : ?>
            <div class="tabs-panel" id="panel5">
              <?php print render($content['field_news_and_insights']);?>
            </div>
          <?php endif ;?>
      </div>
  </div>
</div>
<div class="right-sidebar">
    <p class="email"><?php print render($content['field_email_contact']);?></p>
    <p class="phone"><?php print render($content['field_telephone']);?></p>
    <p class="fax"><?php print render($content['field_fax']);?></p>
    <p class="location"><?php print render($content['field_office']);?></p>
    <p class="education"><?php print render($content['field_law_school']);?></p>
    <?php if (!empty($content['field_vcard_attachment'])) : ?>
      <p class="document"><a href="<?php print (file_create_url($content['field_vcard_attachment']['#items'][0]['uri']));?>"><?php print t('Download pdf');?></a></p>
      <a href="<?php global $base_url; print $base_url . '/attorney/vcard/' . $nid;?>"><?php print t('Download VCard');?></a>
    <?php endif;?>
</div>

  <?php
    // We hide the comments, tags and links now so that we can render them later.
    hide($content['comments']);
    hide($content['links']);
    hide($content['field_tags']);
    hide($content['field_link']);
    hide($content['field_vcard_attachment']);
    print render($content);
  ?>
  <?php if (!empty($content['field_tags']) || !empty($content['links'])): ?>
    <?php print render($content['field_tags']); ?>
    <?php print render($content['links']); ?>
  <?php endif; ?>  


<?php endif; ?>
