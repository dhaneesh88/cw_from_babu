<?php

/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct url of the current node.
 * - $display_submitted: whether submission information should be displayed.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type, i.e., "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type, i.e. story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode, e.g. 'full', 'teaser'...
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined, e.g. $node->body becomes $body. When needing to access
 * a field's raw values, developers/themers are strongly encouraged to use these
 * variables. Otherwise they will have to explicitly specify the desired field
 * language, e.g. $node->body['en'], thus overriding any language negotiation
 * rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 */
?>
<?php if ($view_mode == 'teaser'): ?>



<?php else: ?>
  <?php print render($content['field_home_banner']);?>
  <section class="company-description">
    <?php print render($content['field_signpost']);?>
  </section>

  <section class="twin-blocks relative">
    <div class="left-block featured-industries">
      <h2 class="color-white"><?php print t('Featured Industries');?></h2>
      <ul>
        <?php print render($content['field_featured_industries']);?>
        <li>
          <div class="see-more"><a href="/industries" class="black-btn"><?php print t('Learn More');?></a></div>
        </li>
      </ul>
    </div>
  </section>

  <section class="row max-width">
    <div class="our-offices small-12 large-6 columns">
      <h2 class="blackheading"><?php print t('Our Offices');?></h2>
      <?php print views_embed_view('offices', $display_id = 'panel_pane_4');?>
    </div>
    <div class="map small-12 large-6 columns nopadding" id="jquery_ajax_load_target">
      <?php print views_embed_view('offices', $display_id = 'panel_pane_6');?>
    </div>
  </section>

  <section class="journals">
      <div class="row">
          <div class="small-12 large-4 columns">
              <h2 class="color-white"><?php print t('C&W Journal');?></h2>
              <p class="color-white"><?php print t('Insights and analysis from our attorneys');?></p>
              <div class="view-more"><a href="/cw-journal" class="black-btn"><?php print t('Read More');?></a></div>
          </div>

          <div class="small-12 large-8 columns">
            <h5><?php print render($content['field_journal_edition_date']);?></h5>

            <?php print views_embed_view('journals', $display_id = 'panel_pane_2');?>

            <div id="journals-prev" class="slider-controls prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></div>
            <div id="journals-next" class="slider-controls next"><i class="fa fa-chevron-right" aria-hidden="true"></i></div>

          </div>

      </div>
  </section>
  <section class="news">
      <div class="row">
          <div class="small-12 large-4 columns">
              <h2 class="color-blue"><?php print t('News');?></h2>
              <p><?php print t('The latest firm news and developments');?></p>
              <div class="view-more"><a href="/news" class="blue-btn"><?php print t('Read More');?></a></div>
          </div>
          <div class="small-12 large-8 columns">
              <div class="news-blocks row">
                  <div class="news-block-left large-6 small-12 columns scrollItem">
                    <?php print views_embed_view('news', $display_id = 'panel_pane_4');?>
                  </div>
                  <div class="news-block-right large-6 small-12 columns scrollItem">
                    <?php print views_embed_view('news', $display_id = 'panel_pane_2');?>
                  </div>
              </div>
          </div>
          <div class="slider-controls next" id="arrowRight"><i class="fa fa-chevron-right" aria-hidden="true"></i></div>
          <div class="slider-controls prev" id="arrowLeft"><i class="fa fa-chevron-left" aria-hidden="true"></i></div>
      </div>
  </section>

  <?php
    // We hide the comments, tags and links now so that we can render them later.
    hide($content['comments']);
    hide($content['links']);
    hide($content['field_tags']);
    print render($content);
  ?>
  <?php if (!empty($content['field_tags']) || !empty($content['links'])): ?>
    <?php print render($content['field_tags']); ?>
    <?php print render($content['links']); ?>
  <?php endif; ?>

<?php endif; ?>
