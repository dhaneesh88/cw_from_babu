<header>
<div class="top-bar">
    <div class="top-bar-title">
        <span data-responsive-toggle="responsive-menu" data-hide-for="large">
            <button class="menu-icon dark" type="button" data-toggle></button>
        </span>
        <div class="top-bar-title site-logo">
          <a href="<?php print $front_page;?>"><?php print $site_name;?></a>
        </div>
    </div>
    <div id="responsive-menu">
        <div class="top-bar-right">
            <?php
              print theme('links__system_main_menu', array('links' => $main_menu, 'attributes' => array('id' => 'main-menu', 'class' => array('links')), 'heading' => array('text' => t('Menu'), 'level' => 'h2', 'class' => array('element-invisible'))));
            ?>
            <?php print render($page['search']);?>
        </div>
    </div>
</div>
</header>

<div id="wrapper" class="animated fadeIn">

  <?php $tab_rendered = render($tabs); ?>
  <?php if ($page['highlighted'] || $messages || !empty($tab_rendered) || $action_links): ?>
    <div class="row">
      <div class="large-12 columns" id="admin-functions">
        <?php if ($page['highlighted']): ?>
          <div id="highlighted"><?php print render($page['highlighted']); ?></div>
        <?php endif; ?>
        <?php print $messages; ?>
        <?php if (!empty($tab_rendered)): ?><div class="tabs clearfix"><?php print $tab_rendered; ?></div><?php endif; ?>
        <?php print render($page['help']); ?>
        <?php if ($action_links): ?>
          <ul class="action-links"><?php print render($action_links); ?></ul>
        <?php endif; ?>
      </div>
    </div>
  <?php endif; ?>
</div><!--#wrapper ends-->

<?php print render($page['content']); ?>

<footer id="footer" role="contentinfo">
  <div class="row">
    <div class="small-12 medium-12 large-4 columns footer-section-1"></div>
    <div class="small-12 text-center columns footer-section-2">
      <a href="#"><?php print t('CONTACT US');?></a>
    </div>
    <div class="small-12 large-2 medium-3 columns footer-section-3">
      <?php print render($page['footer_menu_left']); ?>
    </div>
    <div class="small-12 large-2 medium-3 columns footer-section-3">
      <?php print render($page['footer_menu_center']); ?>
    </div>
    <div class="small-12 large-2 medium-3 columns footer-section-3">
      <?php print render($page['footer_menu_right']); ?>
    </div>
    <div class="small-12 large-2 medium-3 columns text-center footer-section-4">
      <?php print render($page['footer_social']); ?>
    </div>
  </div>
  <div class="text-center">
    <?php if(isset($page['custom_copyright'])):?>
      <p class="copyright"><?php print render($page['custom_copyright']);?></p>
    <?php endif;?>
  </div>
</footer>

<div class="hidden">
  <?php print render($page['hidden']); ?>
</div>
