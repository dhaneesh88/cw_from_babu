<?php

/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct url of the current node.
 * - $display_submitted: whether submission information should be displayed.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type, i.e., "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type, i.e. story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode, e.g. 'full', 'teaser'...
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined, e.g. $node->body becomes $body. When needing to access
 * a field's raw values, developers/themers are strongly encouraged to use these
 * variables. Otherwise they will have to explicitly specify the desired field
 * language, e.g. $node->body['en'], thus overriding any language negotiation
 * rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 */
?>

<?php if ($view_mode == 'banner'): ?>

  <div class="banner-image">
    <?php print render($content['field_banner_image']);?>
  </div>
  <?php if (!empty($content['body']['#items'][0]['safe_summary'])) : ?>
    <?php print ($content['body']['#items'][0]['safe_summary']);?>
  <?php endif ;?>

<?php elseif ($view_mode == 'teaser'): ?>
<?php //adding trimmered title #LC-69
 $trimmered_title = truncate_utf8($title, 40, TRUE, TRUE);
?>
    <div class="item-header matchHeight">
        <h3><a href="<?php print $node_url;?>" title="<?php print $title; ?>"><?php print $trimmered_title; ?></a></h3>
        <p><b><?php print render($content['field_tags']); ?></b><?php print render($content['field_date']); ?></p>
    </div>
    <hr>
    <div class="item-content">
        <p><?php print render($content['body']);?></p>
    </div>

<?php elseif ($view_mode == 'listing'): ?>
  <?php if ($variables['view']->row_index == 0) : ?>
  <div class="row first-row">
    <div class="large-12 columns">
        <div class="journal large">
            <div class="label-and-date">
                <label><?php print render($content['field_tags']); ?></label><?php print render($content['field_date']); ?>
            </div>
            <h3><?php print $title; ?></h3>
            <div class="overflow-hidden relative">
              <div class="profile">
                <?php print render($content['field_attorney']); ?>
                <div class="view-more">
                  <a href="<?php print $node_url;?>" class="blue-btn"><?php print t('Continue reading');?></a>
                </div>
              </div>
              <div class="content">
                  <p><?php print render($content['body']);?></p>
                  <div class="view-more">
                    <a href="<?php print $node_url;?>" class="blue-btn"><?php print t('Continue reading');?></a>
                  </div>
              </div>
            </div>
        </div>
    </div>
  </div>
  <?php else : ?>
  <div class="row small-row">

          <div class="journal small">
            <div class="label-and-date">
                <label><?php print render($content['field_tags']); ?></label><?php print render($content['field_date']); ?>
            </div>
            <h3><?php print $title; ?></h3>
            <div>
              <div class="profile">
                <?php print render($content['field_attorney']); ?>
                <div class="view-more">
                  <a href="<?php print $node_url;?>" class="blue-btn"><?php print t('Continue reading');?></a>
                </div>
              </div>
              <div class="content">
                  <p><?php print render($content['body']);?></p>
                  <div class="view-more">
                    <a href="<?php print $node_url;?>" class="blue-btn"><?php print t('Continue reading');?></a>
                  </div>
              </div>
            </div>
        </div>

  </div>
  <?php endif ;?>

<?php else: ?>

  <div class="left-sidebar">
      <?php print render($content['field_attorney']); ?>
      <span><?php if (!empty($content['field_position'])) :?><?php print render($content['field_position']);?> at <?php endif;?><?php print render($content['field_office']);?></span>
      <div class="m-b-30">
        <label><?php print t('Practice Area(s)');?></label>
        <?php print render($content['field_rel_practice_areas']); ?>
      </div>
      <div class="m-b-30">
        <label><?php print t('Journal Edition');?></label>
        <span class="date"><?php print render($content['field_journal_edition']); ?></span>
      </div>
      <div class="m-b-30">
        <?php if (!empty($content['field_attachment'])) :?>
          <div class="attachment">
            <label><?php print t('Attachment');?></label>
            <?php print render($content['field_attachment']); ?>
          </div>
        <?php endif;?>
      </div>
  </div>
  <div class="content-right">
      <p class="news-date"><?php print render($content['field_tags']); ?><?php if (!empty($content['field_date'])) : ?> | <b><?php print render($content['field_date']); ?></b><?php endif;?></p>
      <h2><?php print $title; ?></h2>
      <div class="content">
        <?php print render($content['body']); ?>

        <?php if (!empty($content['field_what_means'])) : ?>
          <h5><?php print t('What It Means to You');?></h5>
          <?php print render($content['field_what_means']); ?>
        <?php endif ;?>
      </div>
      <div class="references">
      <?php if (!empty($content['field_source'])) : ?>
        <div class="m-b-30"><?php print t('Sources');?></div>
        <?php print render($content['field_source']); ?>
      <?php endif ;?>
      </div>
  </div>

  <?php
    // We hide the comments, tags and links now so that we can render them later.
    hide($content['comments']);
    hide($content['links']);
    hide($content['field_banner_image']);
    print render($content);
  ?>

<?php endif; ?>
