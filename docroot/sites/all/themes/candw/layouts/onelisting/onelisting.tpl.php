
<?php if($content['banner']): ?>
  <section class="banner">
	<?php print $content['banner']; ?>
  </section>
<?php endif; ?>

<?php if($content['main']): ?>
  <div class="listing-page" id="wrapper">
    <div class="content-wrapper">
      <?php print $content['main']; ?>
    </div>
  </div>
<?php endif; ?>
