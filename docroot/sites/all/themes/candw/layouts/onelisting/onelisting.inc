<?php

// Plugin definition
$plugin = array(
  'title' => t('One column - listings -candw'),
  'category' => t('Columns: 1'),
  'icon' => 'onelisting.png',
  'theme' => 'onelisting',
  'regions' => array(
    'banner' => t('Banner'),
    'main' => t('Main content')
  ),
);
