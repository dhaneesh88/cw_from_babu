<?php if($content['banner']): ?>
  <section class="banner">
	<?php print $content['banner']; ?>
  </section>
<?php endif; ?>

<?php if($content['main']): ?>
    <div class="detail-page has-rightsidebar" id="wrapper">

          <?php print $content['main']; ?>
          <?php if($content['rightsidebar']): ?>
            <?php print $content['rightsidebar']; ?>
          <?php endif; ?>
    </div>
<?php endif; ?>

<?php if($content['related']): ?>
  <div class="relative bottom-content-slider">
      <?php print $content['related']; ?>
  </div>
<?php endif; ?>
