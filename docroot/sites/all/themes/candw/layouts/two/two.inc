<?php

// Plugin definition
$plugin = array(
  'title' => t('Two columns - candw'),
  'category' => t('Columns: 2'),
  'icon' => 'two.png',
  'theme' => 'two',
  'regions' => array(
    'banner' => t('Banner'),
  	'main' => t('Main content'),
  	'rightsidebar' => t('Right Sidebar'),
  	'related' => t('Related')
  ),
);
