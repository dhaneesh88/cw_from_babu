<?php if($content['banner']): ?>
  <section class="banner">
	<?php print $content['banner']; ?>
  </section>
<?php endif; ?>
<div class="has-rightsidebar detail-page" id="wrapper">
  <?php if($content['main']): ?>
      <?php print $content['main']; ?>
  <?php endif; ?>
  <?php if($content['related']): ?>
    <div class="right-sidebar">
      <?php print $content['related']; ?>
    </div>
  <?php endif; ?>
</div>
<?php if($content['bottom']): ?>
  <div class="relative bottom-content-slider">
	 <?php print $content['bottom']; ?>
  </div>
<?php endif; ?>
