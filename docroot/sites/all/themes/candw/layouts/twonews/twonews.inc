<?php

// Plugin definition
$plugin = array(
  'title' => t('Two columns - News - candw'),
  'category' => t('Columns: 2'),
  'icon' => 'twonews.png',
  'theme' => 'twonews',
  'regions' => array(
  'banner' => t('Banner'),
  'main' => t('Main content'),
  'related' => t('Related'),
  'bottom' => t('Bottom')
  ),
);
