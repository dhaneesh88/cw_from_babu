<?php if($content['banner']): ?>
  <section class="banner">
	<?php print $content['banner']; ?>
  </section>
<?php endif; ?>

<?php if($content['main']): ?>
    <?php print $content['main']; ?>
<?php endif; ?>

<?php if($content['journals']): ?>
  <section class="journals">
    <?php print $content['journals']; ?>
  </section>
<?php endif; ?>

<?php if($content['news']): ?>
  <section class="news">
    <?php print $content['news']; ?>
  </section>
<?php endif; ?>
