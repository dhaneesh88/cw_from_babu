<?php

// Plugin definition
$plugin = array(
  'title' => t('One column - Home - candw'),
  'category' => t('Columns: 1'),
  'icon' => 'onehome.png',
  'theme' => 'onehome',
  'regions' => array(
    'banner' => t('Banner'),
  	'main' => t('Main content'),
  	'journals' => t('Journal list'),
  	'news' => t('News List')
  ),
);
