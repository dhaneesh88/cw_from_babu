<?php

// Plugin definition
$plugin = array(
  'title' => t('One column - candw'),
  'category' => t('Columns: 1'),
  'icon' => 'one.png',
  'theme' => 'one',
  'regions' => array(
    'banner' => t('Banner'),
  	'main' => t('Main content'),
  	'rightsidebar' => t('Right Sidebar'),
  	'related' => t('Related')
  ),
);
