
<?php if($content['banner']): ?>
  <section class="banner">
	<?php print $content['banner']; ?>
  </section>
<?php endif; ?>

<?php if($content['main']): ?>
  <?php if (isset($variables['nodeCT']) && $variables['nodeCT'] == 'attorney') :?>
    <div class="detail-page has-rightsidebar" id="wrapper">
    <?php else : ?>
      <div class="detail-page" id="wrapper">
    <?php endif ;?>
        <div class="content-wrapper">
          <?php print $content['main']; ?>
        </div>
        <?php if($content['rightsidebar']): ?>
          <div class="right-sidebar">
              <?php print $content['rightsidebar']; ?>
          </div>
        <?php endif; ?>
    </div>
<?php endif; ?>

<?php if($content['related']): ?>
  <div class="relative bottom-content-slider">
      <?php print $content['related']; ?>
  </div>
<?php endif; ?>
