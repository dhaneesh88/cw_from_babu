$(document).ready(function () {

    $("#journals-slider").owlCarousel({
        items: 2,
        stagePadding: 10,
        margin: 30,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            1000: {
                items: 2
            }
        }
    });

    // Custom Navigation Events
    $("#journals-next").click(function () {
        $("#journals-slider").trigger('next.owl.carousel');
    })
    $("#journals-prev").click(function () {
        $("#journals-slider").trigger('prev.owl.carousel');
    })

});