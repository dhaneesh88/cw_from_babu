(function ($) {

    /*
        1. Set everything up as variables, e.g:
     
            myFunction = function(){
                var saveEverythingAsVariables = $('#everything');

                ... jquery in here
            }
     
        2. Then add function to required launcher, e.g:

            Ready = function(){
                myfunction();
            }
    */

    var
        // General purpose vars
        htmlBody = $('html,body'),
        Window = $(window),
        Document = $(document),
        Wrapper = $('#wrapper'),
        IndustrySlider = $('#industry-slider'),
        IndustryNxt = $('#industry-next'),
        IndustryPrev = $('#industry-prev'),
        Practiceslider = $('#practise-slider'),
        PracticeNxt = $('#practise-next'),
        PracticePrev = $('#practise-prev'),
        Journalslider = $('#journals-slider'),
        JournalNxt = $('#journals-next'),
        JournalPrev = $('#journals-prev'),
        ContactFormInput = $('.page-contact-open .inputs input'),

        // Make all external links open in new window
        externalLinks = function () {
            var extLink = Wrapper.find($('a[href^="http"],a[href^="https"]'));
            extLink.attr('target', '_blank');
        },

        foundationCall = function () {
            Document.foundation();
        },

        owlSlider = function () {
            IndustrySlider.owlCarousel({
                items: 3,
                stagePadding: 0,
                margin: 30,
                responsive: {
                    0: {
                        items: 1
                    },
                    600: {
                        items: 3
                    },
                    1000: {
                        items: 3
                    }
                }
            });
            // Custom Navigation Events
            IndustryNxt.click(function () {
                IndustrySlider.trigger('next.owl.carousel');
            });
            IndustryPrev.click(function () {
                IndustrySlider.trigger('prev.owl.carousel');
            })

        },

        slideJournals = function () {
            Journalslider.owlCarousel({
                items: 2,
                stagePadding: 0,
                margin: 30,
                responsive: {
                    0: {
                        items: 1
                    },
                    600: {
                        items: 2
                    },
                    1000: {
                        items: 2
                    }
                }
            });
            // Custom Navigation Events
            JournalNxt.click(function () {
                Journalslider.trigger('next.owl.carousel');
            });
            JournalPrev.click(function () {
                Journalslider.trigger('prev.owl.carousel');
            })

        },

        owlCarouselfn = function () {
            Practiceslider.owlCarousel({
                items: 5,
                stagePadding: 0,
                margin: 30,
                responsive: {
                    0: {
                        items: 1
                    },
                    600: {
                        items: 3
                    },
                    1000: {
                        items: 5
                    }
                }
            });
            // Custom Navigation Events
            PracticeNxt.click(function () {
                Practiceslider.trigger('next.owl.carousel');
            });
            PracticePrev.click(function () {
                Practiceslider.trigger('prev.owl.carousel');
            })
        },

        ContactInputs = function () {
            ContactFormInput.bind("keyup", function (e) {
                if ($(this).val() == '') {
                    $(this).next().show();
                } else {
                    $(this).next().hide();
                }
            })
        },

        // Ajax page loading
        ajaxLoading = function () {
            if ($.support.pjax) {
                Document.pjax('a:not(a[href^="http"],a[href^="mailto"],a[target="_blank"], #admin-menu-account a)', Wrapper);

                Document.on('pjax:clicked', function () {
                    Wrapper.addClass('animated fadeOutUp');
                    htmlBody.velocity("scroll", { duration: 350, easing: "ease-in-out" });
                });
            }
        },

        // Smooth scrolling
        smoothScroll = function () {
            Wrapper.on('click', '.scroll', function () {
                var $target = $(this.hash);
                $target.velocity("scroll", { duration: 300, offset: -60, easing: "ease-in-out" });
                return false;
            });
        },

        horizontalScroll = function () {
            var scrollItem = $('.scrollItem'),
                scrollVisible = 1,
                scrollIndex = 0,
                scrollEndIndex = $('.scrollItem').length - 1,
                RightArrow = $('#arrowRight'),
                LeftArrow = $('#arrowLeft');

            RightArrow.click(function () {
                if (scrollIndex < scrollEndIndex) {
                    scrollIndex++;
                    scrollItem.animate({ 'left': '-=' + (window.innerWidth - 45) });
                }
            });

            LeftArrow.click(function () {
                if (scrollIndex > 0) {
                    scrollIndex--;
                    scrollItem.animate({ 'left': '+=' + (window.innerWidth - 45) });
                }
            });
        },

        // Mobile nav
        mobileNav = function () {
            var menuToggle = $('#nav-toggle'),
                nav = $('#main-menu');

            menuToggle.on('click', function () {
                nav.slideToggle(250);
                $(this).toggleClass('active');
                return false;
            });
        },

        ShowSearch = function () {
            var searchicon = $('.top-bar .search-icon');
            searchicon.on('click', function () {
                if (window.innerWidth > 1024) {
                    $(".top-bar .block-search").toggleClass("show");
                }
            });
        },


        // ---- 
        // Prep functions to run
        // ----

        Ready = function () {
            externalLinks();
            //             ajaxLoading();
            //             smoothScroll();
            mobileNav();
            foundationCall();
            owlSlider();
            owlCarouselfn();
            ContactInputs();
            horizontalScroll();
            slideJournals();
            ShowSearch();
        },

        Load = function () {

        },

        Unload = function () {

        };

    // ----
    // Run everything
    // ----

    // Document.ready
    $(function () {
        Ready();
    });

    // When page is loaded
    Window.load(function () {
        Load();
    });

    // When leaving page
    Window.unload(function () {
        Unload();
    });

})(jQuery);
