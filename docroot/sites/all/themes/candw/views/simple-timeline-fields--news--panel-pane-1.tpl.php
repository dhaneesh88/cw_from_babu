<?php
/**
 * @file
 * simple-timeline-fields.tpl.php
 * Created by JetBrains PhpStorm.
 * User: alan
 *
 * @var $simple_timeline_image
 * @var $simple_timeline_date
 * @var $simple_timeline_text
 */
?>

<div class="year"><?php echo $simple_timeline_date; ?><span class="round"></span></div>
<div class="content"><?php echo $simple_timeline_text; ?></div>

