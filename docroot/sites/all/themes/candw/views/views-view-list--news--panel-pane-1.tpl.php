<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 * TIMELINE
 * @ingroup views_templates
 */
?>
<ul>
  <?php foreach ($rows as $id => $row): ?>
      <li class="scrollItem"><?php print $row; ?></li>
  <?php endforeach; ?>
</ul>
