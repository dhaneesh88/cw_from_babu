<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>


  <div class="text-center heading">
    <h5><?php print t('Attorneys working on this Practice Area'); ?></h5>
  </div>
  <div class="row" id="practise-slider">
    <?php foreach ($rows as $id => $row): ?>

        <?php print $row; ?>

    <?php endforeach; ?>
  </div>
  <div id="practise-prev" class="slider-controls prev"><i class="fa fa-chevron-left" aria-hidden="true "></i></div>
  <div id="practise-next" class="slider-controls next"><i class="fa fa-chevron-right" aria-hidden="true "></i></div>

