<?php

/**
 * @file
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */
?>
<div class="grid">
    <div class="right">
        <div class="right-top">
            <div class="row">
                <div class="img-section medium-5 small-5 large-5 columns">
                    <?php print render($fields['field_image']->content); ?>
                </div>
                <div class="content large-7 columns medium-7 small-7">
                    <h4><?php print render($fields['title']->content); ?></h4>
                    <p><?php print render($fields['field_address_map_1']->content); ?></p>
                    <div class="phone">
                        <?php print render($fields['field_telephone']->content); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="right-bottom">
            <div class="map-section">
                <?php print render($fields['field_address_map']->content); ?>
            </div>
        </div>
    </div>
</div>
