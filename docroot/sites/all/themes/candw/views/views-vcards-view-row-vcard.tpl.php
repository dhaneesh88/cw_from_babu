<?php

/**
 * @file
 * Default view template to display a vCard item.
 *
 * The following variables are available:
 * $first_name
 * $middle_name
 * $last_name
 * $full_name
 * $title
 * $email
 * $email2
 * $email3
 * $home_address
 * $home_city
 * $home_state
 * $home_zip
 * $home_country
 * $home_phone
 * $home_cellphone
 * $home_website
 * $work_title
 * $work_company
 * $work_address
 * $work_city
 * $work_state
 * $work_zip
 * $work_country
 * $work_phone
 * $work_fax
 * $work_website
 */
?>
BEGIN:VCARD<?php echo "\n"; ?>
VERSION:3.0<?php echo "\n"; ?>
FN:<?php echo $full_name; ?> <?php echo "\n"; ?>
EMAIL;INTERNET:<?php echo $email; ?><?php echo "\n"; ?>
ORG:<?php echo $work_company; ?><?php echo "\n"; ?>
TEL;WORK:<?php echo $work_phone; ?><?php echo "\n"; ?>
TEL;FAX;WORK:<?php echo $work_fax; ?><?php echo "\n"; ?>
TITLE:<?php echo $work_title; ?><?php echo "\n"; ?>
END:VCARD<?php echo "\n"; ?>
