<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<div class="tabs-content" data-tabs-content="journal-tabs">
  <div class="tabs-panel is-active" id="panel1">
    <?php if (!empty($title)): ?>
      <h3><?php print $title; ?></h3>
    <?php endif; ?>
    <?php foreach ($rows as $id => $row): ?>
          <?php print $row; ?>
    <?php endforeach; ?>
  </div>
</div>
